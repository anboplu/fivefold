#include <sstream>

#include "GuiGen.h"

namespace Gui
{
    // Known information about exception
    std::string MyException::GetHeader() const
    {
        return "Class " + strClsName + ": " + strExcName + " in " + strFnNameTrack;
    }

    // sets exception name (can be used in inherited classes)
    void MyException::ResetName(crefString istrExcName)
    {
        strExcName = istrExcName;
    }

    MyException::MyException(crefString strExcName_, crefString strClsName_, crefString strFnName_)
        :strExcName(strExcName_), strClsName(strClsName_), strFnNameTrack(strFnName_ + "()"), strInherited(""){}

        
    // adds function name to the track list
    void MyException::AddFnName(crefString strFnName)
    {
        strFnNameTrack = strFnName + "()\\" + strFnNameTrack;
    }

    // remembers description of preceding exceptions
    void MyException::InheritException(const MyException& crefExc)
    {
        strInherited = crefExc.GetDescription(true) + "\n" + strInherited;
    }

    // gets description of the exception (short version)
    std::string MyException::GetDescription(bool bDetailed/* = false*/) const
    {
        if(bDetailed)
            return GetHeader() + "\n" + GetErrorMessage() + "\n" + strInherited;
        else
            return "Exception: " + GetErrorMessage() + "\n";
    }

    std::string ColorToString(Color c)
    {
        std::ostringstream ostr;
        ostr << "(" << c.R << ", " << c.G << ", " << c.B << ")";        // Looks like (255, 0, 77)
        return ostr.str();
    }

    std::string RectangleToString(Rectangle r)
    {
        std::ostringstream ostr;
        ostr << "(" << r.p.x << ", " << r.p.y << ", " << r.sz.x << ", " << r.sz.y << ")";   // Looks like (10, 0, 5, 5)
        return ostr.str();
    }


    std::string MatrixErrorInfo::GetErrorMessage() const
    {
        std::ostringstream ostr;
        ostr << "Point (" << p.x << ", " << p.y << ") is out of bounds in " << sz.x << "x" << sz.y << " matrix";
        return ostr.str();
    }


    void Image::SetPixelSafe(Point p, const Color& c)
    {
        if(!InsideRectangle(Rectangle(sz), p))
            throw ImageMatrixException("Image", "SetPixelSafe", sz, p);
        SetPixel(p, c);
    }

    Color Image::GetPixelSafe(Point p) const
    {
        if(!InsideRectangle(Rectangle(sz), p))
            throw ImageMatrixException("Image", "GetPixelSafe", sz, p);
        return GetPixel(p);
    }

    void Image::ChangeColor(const Color& cFrom, const Color& cTo)
    {
        Point p;
        
        for(p.y = 0; p.y < sz.y; ++p.y)
        for(p.x = 0; p.x < sz.x; ++p.x)
            if(GetPixel(p) == cFrom)
                SetPixel(p, cTo);
    }

    void Image::SetTransparentColor(const Color& c)
    {
        ChangeColor(c, Color(0,0,0,0));
    }

    void Image::ColorTransparent(const Color& c)
    {
        ChangeColor(Color(0,0,0,0), c);
    }

    void AdjustImageOverlap(Size sz1, Size sz2, Point& p, Rectangle& r)
    {
        Rectangle rTop = Intersect(r, Rectangle(sz2));
        p = p + rTop.p - r.p;
        Rectangle rBot = Intersect(Rectangle(sz1), Rectangle(p, rTop.sz));
        r = Rectangle(rTop.p + rBot.p - p, rBot.sz);
        p = rBot.p;
    }

    void GuiSaveImage(std::ostream* pStr, const Image* pImg)
    {
        PointerAssert<SimpleException>("<global>", "GuiSaveImage", "pStr", pStr);
        PointerAssert<SimpleException>("<global>", "GuiSaveImage", "pImg" , pImg);
        
        // by some weird reason bfType counts as 4 bytes as opposing to 2, giving an invalid format
        // so we have to write everything by hand
        struct BmpFileHdr       
        {
            unsigned short  bfType;
            unsigned long   bfSize;
            unsigned short  bfReserved1;
            unsigned short  bfReserved2;
            unsigned long   bfOffBits;
        };

        struct BmpInfoHdr
        {
            unsigned long   biSize;
            long            biWidth;
            long            biHeight;
            unsigned short  biPlanes;
            unsigned short  biBitCount;
            unsigned long   biCompression;
            unsigned long   biSizeImage;
            long            biXPelsPerMeter;
            long            biYPelsPerMeter;
            unsigned long   biClrUsed;
            unsigned long   biClrImportant;
        };

        Size sz = pImg->GetSize();
        
        
        BmpFileHdr bmfh;
	    bmfh.bfType = 0x4d42;  // 'BM'
	    bmfh.bfSize = 0;
	    bmfh.bfReserved1 = bmfh.bfReserved2 = 0;
	    //bmfh.bfOffBits = sizeof(BmpFileHdr) + sizeof(BmpInfoHdr);// + sizeof(RGBQUAD) * m_nColorTableEntries;
	    bmfh.bfOffBits = 14 + 40;// + sizeof(RGBQUAD) * m_nColorTableEntries;

        int nFourByteOffset = (4 - sz.x*3 % 4)%4;       // extra zeros for 4byte forced width
        
        BmpInfoHdr bmih;
        bmih.biSize = sizeof(BmpInfoHdr);
        bmih.biWidth = sz.x;
        bmih.biHeight = sz.y;
        bmih.biPlanes = 1;
        bmih.biBitCount = 24;
        bmih.biCompression = 0;
        bmih.biSizeImage = (sz.x*3 + nFourByteOffset)*sz.y;
        bmih.biXPelsPerMeter = bmih.biYPelsPerMeter = bmih.biClrUsed = bmih.biClrImportant = 0;
        
        //pStr->write((char*) &bmfh, sizeof(BmpFileHdr));
        pStr->write((char*) &(bmfh.bfType), 2);
        pStr->write((char*) &(bmfh.bfSize), 4);
        pStr->write((char*) &(bmfh.bfReserved1), 2);
        pStr->write((char*) &(bmfh.bfReserved2), 2);
        pStr->write((char*) &(bmfh.bfOffBits), 4);
            
        //pStr->write((char*) &bmih, sizeof(BmpInfoHdr));
        pStr->write((char*) &(bmih.biSize), 4);
        pStr->write((char*) &(bmih.biWidth), 4);
        pStr->write((char*) &(bmih.biHeight), 4);
        pStr->write((char*) &(bmih.biPlanes), 2);
        pStr->write((char*) &(bmih.biBitCount), 2);
        pStr->write((char*) &(bmih.biCompression), 4);
        pStr->write((char*) &(bmih.biSizeImage), 4);
        pStr->write((char*) &(bmih.biXPelsPerMeter), 4);
        pStr->write((char*) &(bmih.biYPelsPerMeter), 4);
        pStr->write((char*) &(bmih.biClrUsed), 4);
        pStr->write((char*) &(bmih.biClrImportant), 4);
	    
        Point p;
        for(p.y = sz.y - 1; p.y >= 0; --p.y)
        {
            for(p.x = 0; p.x < sz.x; ++p.x)
            {
                Color c = pImg->GetPixel(p);
                pStr->write((char *) &c, 3);
            }
            
            for(int i = 0; i < nFourByteOffset; ++i)
                pStr->write("\0", 1);                   // extra zeros for 4byte forced width
        }

        if(pStr->fail())
            throw SimpleException("<global>", "GuiSaveImage", "Failed to write into the provided stream");
    }

    FontWriter::FontWriter(DrawingHelper<IndexImg> dr_, std::string sFontPath, std::string sFontName)
        :dr(dr_), vImgIndx(256, -1)
    {
        std::ifstream ifs((sFontPath + sFontName).c_str());

        std::string sFontPicture;
        std::getline(ifs, sFontPicture);

        Color cTransp;
        int na, nb, nc;

        ifs >> na >> nb >> nc;
        cTransp.R = na; cTransp.G = nb; cTransp.B = nc;
        ifs >> na >> nb >> nc;
        clSymbol.R = na; clSymbol.G = nb; clSymbol.B = nc;
        
        ifs >> szSymbol.x >> szSymbol.y;
        
        while(ifs.get() != '\n' && !ifs.fail());

        unsigned n;
        unsigned char c;
        for(n = 0; c = ifs.get(), !ifs.fail(); ++n)
            vImgIndx[c] = n;
        
        IndexImg nImg = dr.pGr->LoadImage(sFontPath + sFontPicture);
        Image*   pImg = dr.pGr->GetImage(nImg);
        pImg->ChangeColor(cTransp, Color(0,0,0,0));
        
        for(unsigned i = 0; i < n; ++i)
        {
            IndexImg nCurr = dr.pGr->GetBlankImage(szSymbol);
            Image* pCurr = dr.pGr->GetImage(nCurr);
            
            Point p;
            for(p.y = 0; p.y < szSymbol.y; ++p.y)
            for(p.x = 0; p.x < szSymbol.x; ++p.x)
                pCurr->SetPixel(p, pImg->GetPixelSafe(Point(i * (szSymbol.x + 1) + p.x, p.y)));
            nCurr = dr.pGr->ScaleImage(nCurr, dr.nZoom);
            vImg.push_back(nCurr);
        }
    }
    
    std::string FontWriter::GetNumber(int n, unsigned nDigits/* = 0*/)
    {
        std::string s;
        std::ostringstream ostr(s);
        ostr << n;
        std::string sRes = ostr.str();
        if(sRes.length() >= nDigits)
            return sRes;
        return std::string(nDigits - sRes.length(), '0') + sRes;
    }

    void FontWriter::DrawColorWord(std::string s, Point p, Color c/* = Color(0,0,0,0)*/, bool bCenter/* = false*/, bool bRefresh/* = false*/)
    {
        if(bCenter)
        {
            p.x -= (szSymbol.x + 1) * s.length()/2;
            p.y -= szSymbol.y/2;
        }
        
        for(unsigned i = 0; i < s.length(); ++i)
        {
            int n = int(s[i]);
            
            if(vImgIndx[n] == -1)
                continue;
            
            Point pnt(p.x + (szSymbol.x + 1) * i, p.y);
            pnt = dr(pnt);
            if(c.nTransparent == 0)
                dr.pGr->DrawImage(pnt, vImg[vImgIndx[n]], bRefresh);
            else
            {
                IndexImg vColImg = dr.pGr->CopyImage(vImg[vImgIndx[n]]);
                dr.pGr->GetImage(vColImg)->ChangeColor(clSymbol, c);
                dr.pGr->DrawImage(pnt, vColImg, bRefresh);
            }
        }
    }

}