#include <stdlib.h>

#include <map>
#include <vector>

#include "GuiSdl.h"
#include "Maze.h"

#include <time.h>
#include <iostream>
#include <sstream>

#define TRUE 1
#define FALSE 0

using namespace Gui;

int main(int argc, char *argv[])
{
    srand( (unsigned)time( NULL ));

	SDL_Event event;

    bool bTrue = true;
    bool bExit = false;
    
    try
    {
        Rectangle sBound = Rectangle(0, 0, 1000, 700);

        SP<SdlGraphicalInterface> pGraph = new SdlGraphicalInterface(sBound.sz);

        SP< GraphicalInterface<IndexImg> > pGr = 
        new SimpleGraphicalInterface<SdlImage*>(pGraph);

        pGr->DrawRectangle(sBound, Color(255,255,255), true);

        SDL_WM_SetCaption("Mazing", NULL);

        Uint32 nTimer = SDL_GetTicks();

        bool bStateDr = false;
        bool bPartial = false;

        //Size szSq = Size(300, 200);
        Size szSq = Size(100, 100);
        //Size szSq = Size(50, 50);
        SquareMaze sq(szSq);
        CubeMaze   cb(Size(20, 20), 5);
        //sq.mtxSpots[Point()].vLinks[1].pPass->bWalled = false;

        Maze mz;
        sq.GetMaze(mz);

        Spot* pSpot = mz.vSpots[rand()%mz.vSpots.size()];

        //AldousBroderMaze(mz);
        //WilsonMaze(mz);
        //PrimMaze(mz);
        //KruskalMaze(mz);
        //RecursiveMaze(mz);
        //HuntKillMaze(mz);
        //GrowingTreeMaze(mz);
        //DivideMaze(sq);
        //EllerMaze(sq);
        //BinaryMaze(sq);
        //SidewinderMaze(sq);
        //SimpleBraidMaze(mz);

        //MaxiLooper(mz, 1);
        //DeadEndRemoverTame(mz, .1, true, 0);
        //BlumpsRemover(sq, false);


        std::vector<Spot*> vFrontiers;
        //PrimMazeInit(vFrontiers, pSpot);

        //std::vector<Wall> vWalls;
        //KruskalMazeInit(mz, vWalls, true);
                
        std::list<Spot*> vStack;
        vStack.push_back(pSpot);

        //std::vector<Spot*> vStack;
        //vStack.push_back(pSpot);

        //std::list<Rectangle> v;
        //DivideMazeInit(sq, v);

        //int nY = 0;
        //EllerMazeInit(sq);

        //int nY = sq.mtxSpots.GetSize().y - 1;

        //std::vector<Wall> vWalls;
        //SimpleBraidMazeInit(mz, vWalls, true);

        //std::list<Spot*> vStack;
        //vStack.push_back(pSpot);

        //for(int k = 0; k < 1000; ++k)
         //   GrowingTreeMazeStep(vStack);


        int nAtOnce = 10;
        int nLvl = 0;

        int nStep = 0;

        std::vector<Spot*> vDead;
        std::vector<Wall> vNoWalls;

        std::string str1;
        std::string str2;

        std::ostringstream strDst(str1);
        std::ostringstream strRvr(str2);

        while(!bExit) {
        //for(float f = 0; f < 1.01; f += .1)
        //{
        //strDst << f << "\t";
        //strRvr << f << "\t";
        //std::cout << f << "\n";

        //for(int s = 0; s < 29; ++s) {
    		
		    if(SDL_GetTicks() - nTimer > 0)
            {
                nTimer = SDL_GetTicks();
                //pCurrControl->Update();

                //mz.Wipe();
                
                //AldousBroderMaze(mz);
                //WilsonMaze(mz);
                //PrimMaze(mz);
                //KruskalMaze(mz);
                //RecursiveMaze(mz);
                //HuntKillMaze(mz);
                //GrowingTreeMaze(mz);
                //DivideMaze(sq);
                //EllerMaze(sq);
                //BinaryMaze(sq);
                //SidewinderMaze(sq);
                //SimpleBraidMaze(mz);

                //DeadEndRemoverTame(mz, f, true, 0);
                //DeadEndRemover(mz, f);
                //SimpleBraidMazeCompletion(mz);
                //BlumpsRemover(sq, false);

                pGr->DrawRectangle(sBound, Color(255,255,255), false);
                
                for(int k = 0; k < nAtOnce; ++k)
                {
                    if(nStep == 0)
                    {
                        bool bRes = true;

                        //AldousBroderMazeStep(pSpot);
                        //bRes = WilsonMazeStep(mz, pSpot);
                        //bRes = PrimMazeStep(vFrontiers, true);
                        //bRes = KruskalMazeStep(mz, vWalls);
                        bRes = RecursiveMazeStep(vStack);
                        //bRes = HuntKillMazeStep(mz, pSpot);
                        //bRes = GrowingTreeMazeStep(vStack);
                        //bRes = DivideMazeStep(sq, v);
                        //bRes = EllerMazeStep(sq, nY);
                        //bRes = SidewinderMazeStep(sq, nY);
                        //bRes = SimpleBraidMazeStep(vWalls);
                        //bRes = RecursiveOpenMazeStep(vStack);

                        if(bRes == false)
                        {
                            nStep = 1;
                            nAtOnce = 10;

                            DeadEndRemoverInit(mz, vDead, .25);

                            continue;
                        }
                    }
                    else if(nStep == 2)
                    {
                        /*
                        bool bRes;

                        bRes = DeadEndRemoverStep(vDead);

                        if(bRes == false)
                        {
                            nStep = 3;
                            nAtOnce = 100;

                            SimpleBraidMazeCompletionInit(mz, vNoWalls);

                            continue;
                        }
                        */

                        PrimMazeInit(vFrontiers, mz);
                        nStep = 3;
                        continue;
                    }
                    else if(nStep == 3)
                    {
                        PrimMazeStep(vFrontiers, true);
                    }
                    else if(nStep == 4)
                    {
                        bool bRes;

                        bRes = SimpleBraidMazeStep(vNoWalls);

                        if(bRes == false)
                            nStep = 5;
                    }
                    else if(nStep == 6)
                    {
                        BlumpsRemover(sq);
                    }
                }
                
                //Draw(sq, pGr, Point(10, 10), Size(10, 10), bStateDr);
                Draw(sq, pGr, Point(10, 10), Size(6, 6), bStateDr, bPartial);
                //Draw(sq, pGr, Point(10, 10), Size(3, 3));
                //Draw(cb, pGr, Point(10, 10), Size(10, 10), nLvl);
                

                /*
                for(int i = 0; i < 20000; ++i)
                {
                    Point p = Point(rand()%sBound.sz.x, rand()%sBound.sz.y);
                    pGr->DrawRectangle(Rectangle(p, Size(1,1)), Color(), false); 
                }
                */

                pGr->RefreshAll();

                //std::cout << MaxDistance(mz).n << "\t";
                //std::cout << DeadEndCounter(mz) << "\n";
                //std::cout << f << "\t" << Loopiness(mz) << "\t" << Loopiness(mz) << "\n";

                //strDst << MaxDistance(mz).n << "\t";
                //strRvr << DeadEndCounter(mz) << "\t";
            }
            
            if( SDL_PollEvent( &event ) )
            {
			    int i = event.type;
                
                if(event.type == SDL_QUIT)
                    break;
                
                SpotPair sp;
			    switch( event.type ){
                    case SDL_QUIT:
                        bExit = true;
                        break;
                    case SDL_KEYDOWN:
                        //pCurrControl->OnKey(event.key.keysym.sym, false);
                        
                        switch(event.key.keysym.sym) {
                            case SDLK_EQUALS:
                              std::cout << "Global: " << nGlobalSuperMegaCounter << "\n"; 
                              break;
                            case SDLK_ESCAPE:
                              bExit = true;
                              break;
                            case SDLK_DOWN:
                              ++nLvl;
                              if(unsigned(nLvl) >= cb.cbSpots.size())
                                  --nLvl;
                              break;
                            case SDLK_UP:
                              --nLvl;
                              if(nLvl < 0)
                                  ++nLvl;
                              break;
                            case 'w':
                                mz.Wipe();
                                SimpleBraidMaze(mz);
                                break;
                            case 'r':
                                BlumpsRemover(sq, false);
                                break;
                            case 's':
                                bStateDr = !bStateDr;
                                break;
                            case 'l':
                                std::cout << "Loop: " << MaxiLooper(mz, 1) << "\n";
                                break;
                            case 'c':
                                CarveMazeSpace(mz, true);
                                LimitPartialMaze(mz);
                                mz.Wipe(true);
                                RecursiveMaze(mz);
                                bPartial = true;
                                break;
                            case ' ':
                                ++nStep;
                                //std::cout << nStep << "\n";
                                break;
                            case '1':
                                sp = MaxDistance(mz, mz);
                                std::cout << "Max Distance: " << sp.n << "\n";
                                FindPath(sp, mz);
                                break;
                            default:
                              break;
                        }
                        break;

                    case SDL_KEYUP:
                        //pCurrControl->OnKey(event.key.keysym.sym, true);
                    case SDL_MOUSEBUTTONDOWN:
                        //wp.OnMouse();
                        break;
                    default:
                        break;
			    }
		    }
	    }
        //strDst << "\n";
        //strRvr << "\n";
        //}

        //std::ofstream ofs("output.txt");
        //ofs << "Distance\n" << strDst.str() << "\n\n";
        //ofs << "River\n" << strRvr.str() << "\n\n";
    }
    catch(MyException& me)
    {
        std::cout << me.GetDescription(true) << "\n";
    }
    //catch(...)
    //{
    //    std::cout << "Unknown error!\n";
    //}

    if(nGlobalSuperMegaCounter != 0)
        std::cout << "Memory Leak: " << nGlobalSuperMegaCounter << "\n"; 

    return 0;
}
