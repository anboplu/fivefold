#pragma once

#include "ofMain.h"
#include "ofxiPhone.h"
#include "ofxiPhoneExtras.h"
#include "ffShapeDrawer.h"
#include "PentagonLinearMenu.h"
#include "Maze.h"
class testApp : public ofxiPhoneApp {
private:
	PentagonLinearMenu* pMenu;
	Game* pGm;
	PentaMaze* pPmz;
	
	bool bRight;
	bool bLeft;
	bool bUp;
	bool bDown;
	
public:
	void setup();
	void update();
	void draw();
	
	void touchDown(ofTouchEventArgs &touch);
	void touchMoved(ofTouchEventArgs &touch);
	void touchUp(ofTouchEventArgs &touch);
	void touchDoubleTap(ofTouchEventArgs &touch);
	
	void keyPressed  (int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y );
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);
	
	float	prevX;
	float	prevY;
};


