#include "testApp.h"
#import <Foundation/Foundation.h>
#include "ffShapeDrawer.h"
#include <iostream>

void NewGame(Game*& pGm, PentaMaze*& pPmz, int nLvl)
{
	
    if(pPmz)
        delete pPmz;
    if(pGm)
        delete pGm;
    
    if(nLvl == 0)
        pPmz = new PentaMaze(FiveCoord(6,0,6,6,0));
    else if(nLvl == 1)
        pPmz = new PentaMaze(FiveCoord(2,1,2,2,0));
    else if(nLvl == 2)
        pPmz = new PentaMaze(FiveCoord(1,1,1,1,1));
    else
        pPmz = new PentaMaze(FiveCoord(2,2,2,2,2));
    pGm = new Game();
	
    pPmz->InitLevel(pGm->mpLevel);
    //std::cout << pGm->mpLevel.size() << "\n";
	
    //pGm->lsPath.push_back(Pentagon(FiveCoord(), 50.F, false, pGm->mpLevel[FiveCoord()]));
    for(map<FiveCoord, WallDef>::iterator itr = pGm->mpLevel.begin(), etr = pGm->mpLevel.end(); itr != etr; ++itr)
        pGm->lsPath.push_back(Pentagon(itr->first, 50.F, (itr->first.GetSum() == 0) ? false : true, itr->second));
    pGm->pGuy = pGm->lsPath.back().fCenter;
    pGm->MarkDistances();
	
    pGm->fcExit = pPmz->fcExit;
}


//--------------------------------------------------------------
void testApp::setup(){	
	// register touch events
	ofRegisterTouchEvents(this);
	
	ofxiPhoneSetOrientation(OFXIPHONE_ORIENTATION_LANDSCAPE_RIGHT);

	ofBackground(122,122,122);
	ofSetFrameRate(30);
	
    pMenu = new PentagonLinearMenu();
	
	
    bRight = bLeft = bUp = bDown = false;
	
    pPmz = 0;
    pGm = 0;
	
    NewGame(pGm, pPmz, 0);
	
}


//--------------------------------------------------------------
void testApp::update(){
	int n = pMenu->updateMenu() ;
    if(n != -1 && !pGm->bExist)
    {
        if(n == 4)
        {
            exit();
            return;
        }
        
        NewGame(pGm, pPmz, n);
        pGm->bExist = true;
    }
	
    if(pGm->bFinished)
    {
        //NewGame(pGm, pPmz);
        pGm->bExist = false;
        pGm->bFinished = false;
        pMenu = new PentagonLinearMenu();
    }
    
    fPoint p;
    
    if(bLeft)
        pGm->Move(fPoint(-1, 0));
    if(bRight)
        pGm->Move(fPoint(1, 0));
    if(bUp)
        pGm->Move(fPoint(0, -1));
    if(bDown)
		pGm->Move(fPoint(0, 1));
}

//--------------------------------------------------------------
void testApp::draw(){
	pMenu->drawMenu();
    pGm->Draw();
}


//--------------------------------------------------------------
void testApp::keyPressed(int key){
    if(key == OF_KEY_LEFT)
        bLeft = true;
    if(key == OF_KEY_RIGHT)
        bRight = true;
    if(key == OF_KEY_UP)
        bUp = true;
    if(key == OF_KEY_DOWN)
        bDown = true;
    if(key == ' ')
        pGm->Pulse();
    if(key == 'x')
        pGm->bFinished = true;
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){
	/*
	if(key == OF_KEY_LEFT)
    {
		pMenu->cycleLeft();
    }
	else if(key == OF_KEY_RIGHT)
    {
		pMenu->cycleRight();
    }
	else if(key == OF_KEY_RETURN)
    {
		pMenu->enterMenu();
    }
	
    if(key == OF_KEY_LEFT)
        bLeft = false;
    if(key == OF_KEY_RIGHT)
        bRight = false;
    if(key == OF_KEY_UP)
        bUp = false;
    if(key == OF_KEY_DOWN)
        bDown = false;*/
}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){
	
}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){
	
}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){
	
}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){
	
}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){
	
}

//--------------------------------------------------------------
void testApp::touchDown(ofTouchEventArgs &touch){
	prevX = touch.x;
	prevY = touch.y;
	
}

//--------------------------------------------------------------
void testApp::touchMoved(ofTouchEventArgs &touch){
	//NSLog(@"touch.x is %f", touch.x);
	//NSLog(@"touch.xspeed is %f", touch.xspeed);		
	
	bLeft = (prevX - touch.x > 5);
	bRight = (prevX - touch.x < -5);
	bUp = (prevY-touch.y > 5);
	bDown = (prevY-touch.y < -5);
}

//--------------------------------------------------------------
void testApp::touchUp(ofTouchEventArgs &touch){
	if(prevX - touch.x > 100)
		pMenu->cycleLeft();
	else if(prevX - touch.x < -100)
		pMenu->cycleRight();
	
	bLeft = false;
	bRight = false;
	bUp = false;
	bDown = false;
}

//--------------------------------------------------------------
void testApp::touchDoubleTap(ofTouchEventArgs &touch){
	pMenu->enterMenu();
	pGm->Pulse();
}
