#include <stdlib.h>

#include <map>
#include <set>
#include <list>
#include <vector>
#include <math.h>

#include <time.h>
#include <iostream>

#include "ofConstants.h"
#include "ffShapeDrawer.h"

#include "Maze.h"

using namespace std;

extern bool bShowAll;
extern bool bShowExit;
extern bool bPathLight;
extern bool bReachLight;
extern bool bWaveLight;
extern bool bHighlight;


bool SameSide(fPoint p1, fPoint p2, fPoint l1, fPoint l2);

struct FiveCoord
{
    std::vector<int> v;
    FiveCoord():v(5, 0){}
    FiveCoord(int a, int b, int c, int d, int e){v.push_back(a);v.push_back(b);v.push_back(c);v.push_back(d);v.push_back(e);}
    int& operator[](int i){return v.at(i);}
    bool operator < (const FiveCoord& f) const {return v < f.v;}
    bool operator == (const FiveCoord& f) const {return v == f.v;}
    int GetSum() const;
};

inline FiveCoord operator - (FiveCoord f1, FiveCoord f2)
{
    for(int i = 0; i < 5; ++i)
        f1[i] -= f2[i];
    return f1;
}

std::ostream& operator << (std::ostream& ostr, FiveCoord f);
std::istream& operator >> (std::istream& istr, FiveCoord& f);

FiveCoord& operator += (FiveCoord& f1, FiveCoord f2);

fPoint GetDir(int n);

struct Side
{
    fPoint p1;
    fPoint p2;
    FiveCoord fcDelta;
};

struct WallDef
{
    vector<bool> v;

    WallDef():v(5, false){}
    WallDef(vector<bool> v_):v(v_){}

    bool operator[](unsigned n){return v.at(n);}
    void Set(unsigned n, bool b){v.at(n) = b;}
};

struct Pentagon
{
    FiveCoord fc;
    float fRad;
    bool bFlipped;
    WallDef vWalls;

    int nAge;
    list<ffShapeDrawerPair> lsPulse;
    
    vector<fPoint> vVert;
    vector<Side> vSides;
    fPoint fCenter;

    Pentagon(FiveCoord fc_, float fRad_, bool bFlipped_, WallDef vWalls_);
    bool CloseToVertice(fPoint p);
    bool CrossingWall(fPoint p, fPoint& pWall);
    FiveCoord CrossSide(fPoint p);
};

Pentagon SimplePenta(FiveCoord fc);

bool Intersect(Pentagon& pent, fPoint p);
bool Intersect(Pentagon& pent1, Pentagon& pent2);

struct Game
{
    fPoint pOffset;

    FiveCoord fcExit;
    bool bFinished;
    int nCountDown;

    bool bMenu;
    int nStatus;

    vector<Pentagon> vAllPentagons;
    
    map<FiveCoord, Pentagon*> mpLevel;

    list<Pentagon*> lsVisible;
    set<Pentagon*> stOther;

    fPoint pGuy;
    float fSpeed;
    
    void DrawPentagon(Pentagon& p, int nPath, int nFrame);
    
    void Draw(float fZoom);
    void Move(fPoint fDir);

    void Move(FiveCoord fc);

    void MarkDistances();
    void Pulse();

    Game(bool bMenu_ = false);

	ofImage imMenuBckg;
};

struct PentaMaze
{
    std::vector<Pass*> v;
    std::map<FiveCoord, Spot*> mp;

    FiveCoord fcEnter;
    FiveCoord fcExit;

    FiveCoord fcBnd;
    FiveCoord fcWeight;

    Spot* GetSpot(FiveCoord fc);

    PentaMaze(FiveCoord fcBnd_, FiveCoord fcWeight_ = FiveCoord(1, 1, 1, 1, 1), bool bFillOut = false);
    ~PentaMaze();

    void PassAssignment();
    void SpotPassAssignment(FiveCoord fc, Spot* pS);

    void PartialMaze();

    void PlaceEndpoints(bool bRand = false);

    void GetMaze(Maze& mz);

    void InitLevel(std::map<FiveCoord, WallDef>& mpLevel);

    bool OutOfBound(FiveCoord fc);
};

struct PentaMazeFrontier
{
    std::map<Spot*, FiveCoord> mpFr;

    void Clear();
};

void AdjustPentaFrontiers(PentaMaze* pPmz, PentaMazeFrontier& pmf, Spot* pSpot, Maze& mz, int nClump = -1);

FiveCoord RandomGrid2D(int nL, bool bReg);
FiveCoord RandomGrid3D(int nL);

void PentaMazeRestrict(PentaMaze& pm, std::vector<Spot*>& vFrontiers);