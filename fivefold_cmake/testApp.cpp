#include "testApp.h"
#include "ffShapeDrawer.h"
#include <iostream>

enum Difficulty {DF_TRIVIAL = 0, DF_EASY, DF_MEDIUM, DF_HARD, DF_IMPOSSIBLE};
enum Dimension  {DM_TWO, DM_TWO_THREE, DM_THREE, DM_TWO_THREE_FOUR, DM_THREE_FOUR, DM_FOUR};

FiveCoord Weight_2_3_4(FiveCoord& fc, int nDim_3, int nDim_4, int nW_2, int nW_3, int nW_4)
{
    FiveCoord fcRet;
    bool bFirst = rand()%2 == 0;

    for(int i = 0; i < 5; ++i)
        if(fc[i] != 0)
            fcRet[i] = nW_2;
        else
        {
            if(bFirst)
            {
                fcRet[i] = nW_3;
                fc[i] = nDim_3;
            }
            else
            {
                fcRet[i] = nW_4;
                fc[i] = nDim_4;
            }

            bFirst = !bFirst;
        }

    return fcRet;
}

FiveCoord Weight_2_3(FiveCoord& fc, int nDim_3, int nW_2, int nW_3)
{
    return Weight_2_3_4(fc, nDim_3, 0, nW_2, nW_3, 0);
}

FiveCoord Weight_3_4(FiveCoord& fc, int nDim_4, int nW_3, int nW_4)
{
    FiveCoord fcRet;
    
    for(int i = 0; i < 5; ++i)
        if(fc[i] != 0)
            fcRet[i] = nW_3;
        else
        {
            fcRet[i] = nW_4;
            fc[i] = nDim_4;
        }

    return fcRet;
}

void NewMenu(Game*& pGm, PentaMaze*& pPmz)
{
    if(pPmz)
        delete pPmz;
    if(pGm)
        delete pGm;

    pPmz = new PentaMaze(FiveCoord(1,1,1,1,1));

    for(int i = 0; i < 5; ++i)
    {
        FiveCoord fc(0,0,0,0,0);
        fc[i] = -1;
        Spot* pS = new Spot();
        pS->vLinks.resize(5);
        pPmz->mp[fc] = pS;
    }

    pPmz->PassAssignment();

    Maze mz;
    pPmz->GetMaze(mz);
    mz.Wipe(false);

    pPmz->fcEnter = FiveCoord(0,0,0,0,0);
    pPmz->fcExit = FiveCoord(2,2,2,2,2);
    
    pGm = new Game(true);

    map<FiveCoord, WallDef> mpLevel;
    
    pPmz->InitLevel(mpLevel);

    pGm->vAllPentagons.reserve(mpLevel.size());
    for(map<FiveCoord, WallDef>::iterator itr = mpLevel.begin(), etr = mpLevel.end(); itr != etr; ++itr)
    {
        Pentagon p(itr->first, 50.F, (itr->first.GetSum() == 0) ? false : true, itr->second);

        pGm->vAllPentagons.push_back(p);

        Pentagon* pPnt = & (pGm->vAllPentagons.back());

        pGm->mpLevel[itr->first] = pPnt;
        pGm->stOther.insert(pPnt);
    }


    pGm->fcExit = pPmz->fcExit;
    pGm->Move(pPmz->fcEnter);

    pGm->pGuy = pGm->lsVisible.back()->fCenter;
    pGm->MarkDistances();
}

void NewGame(Game*& pGm, PentaMaze*& pPmz, int nLvl)
{
    if(pPmz)
        delete pPmz;
    if(pGm)
        delete pGm;

    // Level generation
    
    Difficulty df = Difficulty(nLvl);

    bool bSimplified = false;   // use level structure from the next difficulty, maze is simplified

    if(df == DF_MEDIUM || df == DF_HARD)
    {
        if(rand()%5 == 0)
        {
            bSimplified = true;
            df = Difficulty(nLvl + 1);
        }
    }

    int nPartial = -1;      // for partial generation, how big is the maze
    int nClump = -1;        // for partial generation, controls how many tiles are put on top of each other
    
    if(df == DF_TRIVIAL)
    {
        if(rand()%4 != 0)   // partial maze
        {
            if(rand()%3 == 0)
                pPmz = new PentaMaze(RandomGrid2D(4, true));    // 2-dim maze
            else
            {
                pPmz = new PentaMaze(FiveCoord(2,2,2,2,2));     // random non-intersecting maze
                nClump = 1;
            }

            nPartial = 30;
        }
        else        // whole maze
        {
            nPartial = -1;

            pPmz = new PentaMaze(RandomGrid2D(2, true), FiveCoord(1,1,1,1,1), true);
        }
    }
    else if (df == DF_EASY)
    {
        if(rand()%3 != 0)   // partial maze
        {
            if(rand()%4 != 0)   // non-intersecting maze
            {
                if(rand()%3 == 0)
                {
                    pPmz = new PentaMaze(RandomGrid2D(4, true));    // 2-dim maze
                    nPartial = 50;
                }
                else
                {
                    pPmz = new PentaMaze(FiveCoord(2,2,2,2,2));     // non-clumped maze
                    nClump = 1;
                    nPartial = 50;
                }
            }
            else        // intersecting maze
            {
                int n = rand()%3;
                
                if(n == 0)
                {
                    pPmz = new PentaMaze(RandomGrid2D(4, false));   // 2-dim intersecting maze
                    nPartial = 30;
                }
                else if (n == 1)
                {
                    FiveCoord fcG = RandomGrid2D(4, true);          // 2-dim maze with a weak 3rd dimension
                    FiveCoord fcW = Weight_2_3(fcG, 2, 10, 1);
                    pPmz = new PentaMaze(fcG, fcW);
                    nPartial = 30;
                    nClump = 4;
                }
                else
                {
                    pPmz = new PentaMaze(FiveCoord(2,2,2,2,2));     // very low clump factor
                    nClump = 2;
                    nPartial = 30;
                }
            }
        }
        else    // whole maze
        {
            nPartial = -1;

            int n = rand()%4;
            
            if(n == 0)
            {
                FiveCoord fcDim = RandomGrid2D(2, true);
                
                int i_s = rand()%5;
                for(int i = 0; i < 5; ++i)
                    if(fcDim[(i + i_s)%5] == 2)
                    {
                        fcDim[(i + i_s)%5] = 3;
                        break;
                    }
                
                pPmz = new PentaMaze(fcDim, FiveCoord(1,1,1,1,1), true);    // bigger 2-dim maze
            }
            else if (n == 1)
            {
                pPmz = new PentaMaze(RandomGrid2D(2, false), FiveCoord(1,1,1,1,1), true);   // 2-dim interlaping maze
            }
            else
            {
                FiveCoord fcG = RandomGrid2D(1, true);      // very small 2-dim maze with a weak 3rd dimension
                FiveCoord fcW = Weight_2_3(fcG, 1, 10, 1);
                pPmz = new PentaMaze(fcG, fcW, true);
            }
        }
    }
    else if (df == DF_MEDIUM)   // all mazes partial (unless it is simplified hard)
    {
        if(rand()%2 == 0)
        {
            FiveCoord fcG = RandomGrid2D(4, true);
            FiveCoord fcW = Weight_2_3(fcG, 2, 10, 1);      // 2-dim maze with a weak third dimension
            pPmz = new PentaMaze(fcG, fcW);
            nPartial = 70;
        }
        else
        {
            pPmz = new PentaMaze(RandomGrid3D(4));          // small 3d maze
            nPartial = 50;
        }
        nClump = 5;
    }
    else if (df == DF_HARD)
    {
        if(rand()%5 != 0)   // partial maze
        {
            int n = rand()%3;

            if(n == 0)      // 3-dim maze
            {
                pPmz = new PentaMaze(RandomGrid3D(4));
                nPartial = 100;
            }
            else if(n == 1)     // 2-dim maze with weak 3rd dimension, and even weaker 4th dimension
            {
                FiveCoord fcG = RandomGrid2D(4, true);
                FiveCoord fcW = Weight_2_3_4(fcG, 2, 2, 100, 10, 1);
                pPmz = new PentaMaze(fcG, fcW);
                nPartial = 120;
            }
            else    // 3-dim maze with weak 4th dimension
            {
                FiveCoord fcG = RandomGrid3D(4);
                FiveCoord fcW = Weight_3_4(fcG, 2, 10, 1);
                pPmz = new PentaMaze(fcG, fcW);
                nPartial = 80;
            }

            nClump = 6;
        }
        else        // whole maze
        {
            nPartial = -1;
            
            FiveCoord fcG = RandomGrid2D(2, true);      // 2-dim with weak 3-rd dimension
            FiveCoord fcW = Weight_2_3(fcG, 1, 10, 1);
            pPmz = new PentaMaze(fcG, fcW, true);
        }
    }
    else // DF_IMPOSSIBLE
    {
        if(rand()%3 != 0)   // partial maze
        {
            if(rand()%2)
            {
                pPmz = new PentaMaze(FiveCoord(5,5,5,5,5));     // 4-d maze
                nPartial = 100;
            }
            else
            {
                FiveCoord fcG = RandomGrid3D(4);
                FiveCoord fcW = Weight_3_4(fcG, 2, 10, 1);      // 3-d with weak 4th dimension
                pPmz = new PentaMaze(fcG, fcW);
                nPartial = 100;
            }
    
            nClump = 7;
        }
        else        // whole maze
        {
            nPartial = -1;
            
            pPmz = new PentaMaze(FiveCoord(1,1,1,1,1), FiveCoord(1,1,1,1,1), true);     // small 4-d maze
        }

    }

    Maze mz;
    pPmz->GetMaze(mz);

    if(nPartial != -1)
    {
        PentaMazeFrontier pmf;
        pmf.mpFr[mz.vSpots[0]] = FiveCoord(0,0,0,0,0);

        AdjustPentaFrontiers(pPmz, pmf, mz.vSpots[0], mz, nClump);
        
        std::vector<Spot*> vFrontiers;
        PrimMazeInit(vFrontiers, mz.vSpots[0]);

        std::list<Spot*> lStack;
        lStack.push_back(mz.vSpots[0]);

        std::vector<Spot*> vStack;
        vStack.push_back(mz.vSpots[0]);

        Spot* pHuntSpot = mz.vSpots[0];

        int nP = rand()%5;

        int nInit = nPartial/4;
        if(nInit == 0)
            nInit = 1;
        int nFinal = nPartial - nInit;

        for(int i = 0; i < nInit; ++i)
        {
            Spot* pSpot;
            if(nP == 0)
                pSpot = PrimMazeStep(vFrontiers, true);
            else if(nP == 1)
                pSpot = RecursiveMazeStep(lStack);
            else if(nP == 2)
                pSpot = GrowingTreeMazeStep(vStack, 5);
            else if(nP == 3)
                pSpot = HuntKillMazeStep(mz, pHuntSpot);
            else
                pSpot = AldousBroderMazeStep(pHuntSpot);

            if(!pSpot)
                break;

            AdjustPentaFrontiers(pPmz, pmf, pSpot, mz, nClump);
            if(nP == 0)
                FrontierAdjust(vFrontiers, pSpot);
            //std::cout << i << "\n";
        }

        PrimMazeInit(vFrontiers, mz);

        for(int i = 0; i < nFinal; ++i)
        {
            Spot* pSpot = PrimMazeStep(vFrontiers, true);

            if(!pSpot)
                break;
            
            AdjustPentaFrontiers(pPmz, pmf, pSpot, mz, nClump);
            FrontierAdjust(vFrontiers, pSpot);
            //std::cout << i << "\n";
        }


        pPmz->GetMaze(mz);
        mz.Flush(1);
        CarveMazeSpace(mz, true);
        LimitPartialMaze(mz);
        mz.Wipe(true);
        mz.Flush(0);
        pPmz->PartialMaze();
    }

    int n = rand()%7;
    
    if(n == 0)
        WilsonMaze(mz);
    else if (n == 1)
        PrimMaze(mz, true);
    else if (n == 2)
        KruskalMaze(mz, true);
    else if (n == 3)
        RecursiveMaze(mz);
    else if (n == 4)
        GrowingTreeMaze(mz, 10);
    else if (n == 5)
        HuntKillMaze(mz);
    else if (n == 6)
        SimpleBraidMaze(mz, true);

    if(!bSimplified)
    {
        n = rand()%4;

        if(n == 0)
        {
            DeadEndRemover(mz, 1);
            SimpleBraidMazeCompletion(mz);
        }
        else if (n == 1)
        {
            DeadEndRemover(mz, .2);
            
            if(rand()%2 == 0)
                SimpleBraidMazeCompletion(mz);
        }
        else if(n == 2)
            MaxiLooper(mz, 1);
    }
    else
        MaxiLooper(mz, 100, 4);
    
    if(df == DF_TRIVIAL || df == DF_EASY || df == DF_MEDIUM || bSimplified)
        pPmz->PlaceEndpoints(false);
    else
        pPmz->PlaceEndpoints(true);

    //cout << "Loops: " << Loopiness(mz) << "\n";

    pGm = new Game();

    map<FiveCoord, WallDef> mpLevel;
    
    pPmz->InitLevel(mpLevel);

    pGm->vAllPentagons.reserve(mpLevel.size());
    for(map<FiveCoord, WallDef>::iterator itr = mpLevel.begin(), etr = mpLevel.end(); itr != etr; ++itr)
    {
        Pentagon p(itr->first, 50.F, (itr->first.GetSum() == 0) ? false : true, itr->second);

        pGm->vAllPentagons.push_back(p);

        Pentagon* pPnt = & (pGm->vAllPentagons.back());

        pGm->mpLevel[itr->first] = pPnt;
        pGm->stOther.insert(pPnt);
    }


    pGm->fcExit = pPmz->fcExit;
    pGm->Move(pPmz->fcEnter);

    pGm->pGuy = pGm->lsVisible.back()->fCenter;
    pGm->MarkDistances();
}


//--------------------------------------------------------------
void testApp::setup(){
	ofBackground(122,122,122);
	ofSetFrameRate(30);
	ofSetFullscreen(true);

	sndWin.loadSound("WINNER.mp3");
	sndSelect.loadSound("SELECT.mp3");
	sndMusic.loadSound("cube_beat.mp3");

	sndMusic.setLoop(true);
	sndMusic.play();

	imLoading.loadImage("loading.png");

    fZoom = 1.F;

    bRight = bLeft = bUp = bDown = false;

    pPmz = 0;
    pGm = 0;

	bLoad = false;

    NewMenu(pGm, pPmz);
}

testApp::~testApp()
{
    delete pPmz;
    delete pGm;
}



//--------------------------------------------------------------
void testApp::update(){
	
    if(!pGm)
        return;

	if(pGm->nCountDown == 94)
	{
		if(!pGm->bMenu)
			sndWin.play();
		else
			sndSelect.play();
	}

    if(pGm->bFinished)
    {
        if(pGm->bMenu)
        {
			if(!bLoad)
			{

				bLoad = true;
			}
			else
			{
				int n = pGm->nStatus;
	                
				NewGame(pGm, pPmz, (8 - n)%5);

				bLoad = false;
			}
        }
        else
        {
            NewMenu(pGm, pPmz);
        }
    }


    fPoint p(0,0);
    
    if(bLeft)
        p += fPoint(-1, 0);
    if(bRight)
        p += fPoint(1, 0);
    if(bUp)
        p += fPoint(0, -1);
    if(bDown)
       p += fPoint(0, 1);
   pGm->Move(p);
}

//--------------------------------------------------------------
void testApp::draw()
{    
    ofPushMatrix();
    
	if(!bLoad)
	{
		if(pGm)
		{
			if(pGm->bMenu)
				pGm->Draw(1);
			else
				pGm->Draw(fZoom);
		}
	}
	else
	{
		ofPushMatrix();
		
		ofTranslate(ofGetWidth()/2, ofGetHeight()/2);
		ofTranslate(-imLoading.getWidth()/2, -imLoading.getHeight()/2);
		glEnable(GL_BLEND);
		imLoading.draw(0,0);
		
		ofPopMatrix();
	}
	ofPopMatrix();
}

//--------------------------------------------------------------
void testApp::keyPressed(int key)
{
	if(key == OF_KEY_LEFT)
        bLeft = true;
    if(key == OF_KEY_RIGHT)
        bRight = true;
    if(key == OF_KEY_UP)
        bUp = true;
    if(key == OF_KEY_DOWN)
        bDown = true;
    if(pGm)
    {
        if(key == ' ')
            pGm->Pulse();
        if(!pGm->bMenu && key == 'q')
            pGm->bFinished = true;
    }

    if(key == '1')
        bShowAll = !bShowAll;
    if(key == '2')
        bShowExit = !bShowExit;
    if(key == '3')
        bPathLight = !bPathLight;
    if(key == '4')
        bReachLight = !bReachLight;
    if(key == '5')
        bWaveLight = !bWaveLight;
    if(key == '6')
        bHighlight = !bHighlight;

    if(pGm && !pGm->bMenu)
	{
		if(key == 'a')
			fZoom *= 1.1;
		if(key == 's')
			fZoom /= 1.1;
	}
}

//--------------------------------------------------------------
void testApp::keyReleased(int key)
{
    if(key == OF_KEY_LEFT)
        bLeft = false;
    if(key == OF_KEY_RIGHT)
        bRight = false;
    if(key == OF_KEY_UP)
        bUp = false;
    if(key == OF_KEY_DOWN)
        bDown = false;
}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}