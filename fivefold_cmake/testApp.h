#ifndef _TEST_APP
#define _TEST_APP
#include "ofMain.h"
#include "PentagonLinearMenu.h"


#include "PenMaze.h"

class testApp : public ofBaseApp{
    private:
		//PentagonLinearMenu* pMenu;
        Game* pGm;
        PentaMaze* pPmz;

        bool bRight;
        bool bLeft;
        bool bUp;
        bool bDown;

        float fZoom;

		ofSoundPlayer sndWin;
		ofSoundPlayer sndSelect;
		ofSoundPlayer sndMusic;

		ofImage imLoading;

		bool bLoad;

	public:
        ~testApp();

		void setup();
		void update();
		void draw();

		void keyPressed  (int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);

};

#endif
